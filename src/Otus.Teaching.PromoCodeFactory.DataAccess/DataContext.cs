﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Security.Cryptography.X509Certificates;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);

            modelBuilder.Entity<CustomerPromoCode>()
                .HasKey(bc => new { bc.CustomerId, bc.PromoCodeId });
            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.PromoCodes)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(bc => bc.PromoCode)
                .WithMany(bc => bc.Customers)
                .HasForeignKey(bc => bc.PromoCodeId);

            modelBuilder.Entity<Preference>()
            .HasData(FakeDataFactory.Preferences
            );
        }
    }
}