﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class SeedInitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "preferences",
                columns: new[] { "id", "name" },
                values: new object[] { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "Театр" });

            migrationBuilder.InsertData(
                table: "preferences",
                columns: new[] { "id", "name" },
                values: new object[] { new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "Семья" });

            migrationBuilder.InsertData(
                table: "preferences",
                columns: new[] { "id", "name" },
                values: new object[] { new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "Дети" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "preferences",
                keyColumn: "id",
                keyValue: new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"));

            migrationBuilder.DeleteData(
                table: "preferences",
                keyColumn: "id",
                keyValue: new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"));

            migrationBuilder.DeleteData(
                table: "preferences",
                keyColumn: "id",
                keyValue: new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"));
        }
    }
}
