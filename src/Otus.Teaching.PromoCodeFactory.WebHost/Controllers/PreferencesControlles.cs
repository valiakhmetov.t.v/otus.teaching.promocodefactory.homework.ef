﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Converters;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesControlles
        : ControllerBase
    {

        private readonly IRepository<Preference> _preferenceRepository;

        public PreferencesControlles(
            IRepository<Preference> preferenceRepository)
        {

            _preferenceRepository = preferenceRepository;
        } 

        /// <summary>
        /// Получить данные о всех предпочтениях
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerResponse>> GetPreferencesAsync()
        {
            var customers = await _preferenceRepository.GetAllAsync();
            
            var response = customers.Select(x => new PreferenceResponse() {Id = x.Id, Name = x.Name }).ToList();

            return Ok(response);
        }
      
    }
}