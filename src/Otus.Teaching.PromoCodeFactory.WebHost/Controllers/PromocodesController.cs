﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Converters;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository, IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferencesRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var codes = await _promoCodeRepository.GetAllAsync();

            var response = codes.Select(x => new PromoCodeShortResponse(x)).ToList();

            return Ok(response);
        }

        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferencesRepository.FindByCondition(x => x.Name == request.Preference);
            if (preference == null)
            {
                return NotFound();
            }
            var customers = await _customerRepository.FindByCondition(x => x.Preferences.Where(y => y.PreferenceId == preference.First().Id).Any());

            PromoCode newPromocode = PromoCodeConverter.Convert(request, preference.First(), customers);
            foreach(var customer in customers)
            {
                await _customerRepository.UpdateAsync(customer);
            }
            await _promoCodeRepository.AddAsync(newPromocode);

            return Ok(newPromocode.Id);
        }
    }
}