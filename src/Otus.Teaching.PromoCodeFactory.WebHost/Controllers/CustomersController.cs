﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Converters;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository, IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        } 

        /// <summary>
        /// Получить данные о всех клиентах
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            
            var response = customers.Select(x => new CustomerResponse(x)).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Получить данные о клиенте
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
          
            var response = new CustomerResponse(customer);

            return Ok(response);
         
        }
        
        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetAllByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerConverter.Convert(request, preferences);

            await _customerRepository.AddAsync(customer);

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = customer.Id }, customer.Id);
        }

        /// <summary>
        /// Отредактировать данные о клиенте
        /// </summary>
        /// <param name="id">ID клиента</param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetAllByIdsAsync(request.PreferenceIds);

            CustomerConverter.Convert(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удалить клиента вместе с его промокодами
        /// </summary>
        /// <param name="id">ID Клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            await _customerRepository.DeleteAsync(customer);

            var orphanedPromocodes = await _promoCodeRepository.FindByCondition(x => x.Customers.Count == 0);
            foreach (var promocode in orphanedPromocodes)
            {
                await _promoCodeRepository.DeleteAsync(promocode);
            }
            
            return NoContent();
        }
    }
}